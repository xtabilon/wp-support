<?php get_header(); ?>
	 
	
<div id="banner">
    
 		<div class="container">  
			<button type="button" class="btn btn-default">Have a question? Ask here!</button>				
			<button type="button" class="btn btn-primary pull-left">Get Started</button>
		</div>
		         
    </div><!-- /#banner -->
    
    <!-- HOW IT WORKS SECTION -->
    <div class="container how-it-works">
    	<div class="row">
    		<h2>How it Works</h2>
    	</div>
      <div class="row">
        <?php $args = array( 'post_type' => 'post', 'posts_per_page' => 3, 'category_name' => 'how-it-works');
			$wp_query = new WP_Query($args);
        ?>
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <div class="col-md-4">
          <a href="#" class="thumbnail">
            <!--<img src="http://placehold.it/310x310" alt="" class="img-circle img-responsive" />-->
            <?php  the_post_thumbnail( 'how-it-works', array('class' => 'img-circle img-responsive') ); ?>
          </a>
        </div>
        <?php endwhile; ?>
        
        <?php else: ?>
		<article>
			
			<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
			
		</article>
		<!-- /article -->
	
        <?php endif; ?>
        <?php wp_reset_query(); ?>
        <!--<div class="col-md-4">
          <a href="#" class="thumbnail">
            <img src="http://placehold.it/310x310" alt="" class="img-circle img-responsive"/>
          </a>
        </div>
        <div class="col-md-4">
          <a href="#" class="thumbnail">
            <img src="http://placehold.it/310x310" alt="" class="img-circle img-responsive"/>
          </a>
        </div> -->
      </div>

    </div>  <!-- .how-it-works -->

<hr />
    <?php $args = array( 'post_type' => 'post', 'posts_per_page' => 3, 'category_name' => 'example-jobs');
			$wp_query = new WP_Query($args);
	?>
    <!-- EXAMPLE JOB SECTION -->
    <article class="container example-job">
      <div class="row">
        <h2>Example Jobs</h2>
      </div>
      <div class="row">

      <?php if (have_posts()): while (have_posts()) : the_post(); ?>
      
        <div class="col-md-4">
          <div class="border-box" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <a href="<?php the_permalink(); ?>"><h3 class="panel-title"><?php the_title(); ?></h3></a>
            <?php the_content(); ?>
          </div>
        </div>       
        
      <?php endwhile; ?>

      </div>
	
	<?php else: ?>
		<article>
			
			<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
			
		</article>
		<!-- /article -->
	
	<?php endif; ?>
    </article>
    <?php wp_reset_query(); ?>
    <!--PLANS and SIGN UP -->
    <div class="gray-bg">
    <hr />
    <div class="container plans-and-signup">
      <div class="row">
        <h2>Plans and Sign up</h2>
      </div>
      <div class="row">
        <table class="table">
          <tr>
            <th></th>
            <th>Standard $69 monthly/site</th>
            <th>Professional $99 monthly/site</th>
            <th>Expert $139 monthly/site</th>
          </tr>
          <tr>
            <td>24 hour chat support</td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
          </tr>
          <tr>
            <td>Fix notes for peace of mind and your records</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>All jobs tested before going live</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Unlimited small jobs(less than 30 minutes)</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </table>
      </div>
    </div>
    </div>

<?php get_footer(); ?>