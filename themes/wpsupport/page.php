<?php get_header(); ?>
	
<div class="container">
    
<div class="span8 pull-left">
	<div role="main" class="">
	
		<h2><?php the_title(); ?></h2>
	
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	
		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
			<?php the_content(); ?>
			
			
			<br class="clear">
			
			<?php edit_post_link(); ?>
			
		</article>
		<!-- /article -->
		
	<?php endwhile; ?>
	
	<?php else: ?>
	
		<!-- article -->
		<article>
			
			<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
			
		</article>
		<!-- /article -->
	
	<?php endif; ?>
	
	</div>
	<!-- /section -->
</div> 

<div class="span4 pull-right">	
<?php get_sidebar(); ?>
</div><!-- container-->
<div class="clearfix"></div>
</div><!--container-->

<?php get_footer(); ?>