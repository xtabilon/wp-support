<?php
/*
Template Name: Blog Posts
*/
?>
<?php get_header(); ?>

<div class="container">
    <div class="col-md-8">
    
    
    
    <?php query_posts('post_status=publish&posts_per_page=10&paged='. get_query_var('paged')); ?>

	<?php if( have_posts() ): ?>

        <?php while( have_posts() ): the_post(); ?>

            <div id="post-<?php get_the_ID(); ?>" <?php post_class(); ?>>

        

                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
                    <?php the_content(); ?>
                    
                    
                    
                    <?php //edit_post_link(); ?>
                    
                </article>

            </div>

        <?php endwhile; ?>
        
        	

	<?php else: ?>

		<article>
			
			<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
			
		</article>

	<?php endif; wp_reset_query(); ?>
    
    </div>
    <div class="col-md-4">
    <?php get_sidebar(); ?>
    </div>
    <div class="clearfix"></div>
</div>

<?php get_footer(); ?>